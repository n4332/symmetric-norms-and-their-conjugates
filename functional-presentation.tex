\documentclass[letterpaper, 12pt]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb, amsthm}
\usepackage{dsfont} %allows for \mathds{1}
\usepackage{enumitem}
%\usepackage{xcolor}
%\usepackage{hyperref}
%\usepackage{tikz}
%\usepackage{pgfplots}
%\pgfplotsset{compat = newest}


\newtheorem{theorem}{Theorem}[section]
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\theoremstyle{remark}
\newtheorem{remark}[theorem]{Remark}
\theoremstyle{definition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{example}[theorem]{Example}

\newtheoremstyle{named}{}{}{\itshape}{}{\bfseries}{.}{.5em}{\thmnote{#3}}
\theoremstyle{named}
\newtheorem*{namedtheorem}{Theorem}

\newcommand{\N}{\ensuremath{\mathbb{N}}}
\newcommand{\Z}{\ensuremath{\mathbb{Z}}}
\newcommand{\Q}{\ensuremath{\mathbb{Q}}}
\newcommand{\R}{\ensuremath{\mathbb{R}}}
\newcommand{\C}{\ensuremath{\mathbb{C}}}
\newcommand{\T}{\ensuremath{\mathbb{T}}}


\begin{document}
\title{Symmetric Norms and Their Conjugates}
\author{Nicholas Christoffersen}
\date{\today}
\maketitle
    \section{Introduction}
    The exposition below follows from Section 1.7 of Barry Simon's ``Trace Ideals and Their Applications''~\cite{simon2005trace}. A few things have been changed notation-wise, details have been filled out, and minor corrections have been made.

    \section{What is a Symmetric Norm?}
    Recall that $c_{00}(\N)$ denotes the space of finite sequences indexed by the natural numbers, and $c_0(\N)$ denotes the space of sequences that decay to zero, indexed by the natural numbers. Further, for any $a\in c_0(\N)$, the sequence $a^{*}\in c_0(\N)$ is defined such that:
    \begin{align*}
        a_1^{*} &= \max_{i\in \N} |a_i|; \quad 
        a_1^{*} + a_2^{*} = \max_{i\neq j} |a_i| + |a_j|; \quad 
        \ldots
    \end{align*}

    \begin{definition}
        A norm  $\Phi$ on $c_{00}(\N)$ is called \textit{symmetric} if for all $a\in c_{00}(\N)$, 
        \[
            \Phi(a) = \Phi(a^{*}).
        \]
    \end{definition}

    Note that this is the same as saying that $\Phi$ is invariant under permutations and maps of the form:
    \[
        a_n \to e^{i\theta_n}a_n
    \]
    where $\theta_n\in \R$. Now, we extend $\Phi$ to two different sequence spaces.

    \begin{definition}
        Given a symmetric norm $\Phi$, we define the \textit{maximal space} $s_\Phi$ to be the space of all sequences $(a_n)$ such that
        \[
            \Phi(a) := \lim_{n \to \infty} \Phi(a_1,\ldots, a_n, 0, 0, \ldots)
        \]
        exists and is finite. We define the \textit{minimal space} $s_\Phi^{(0)}$ to be the closure of $c_{00}(\N)$ with respect to $\Phi$. If $s_\Phi = s_\Phi^{(0)}$ (i.e. $c_{00}(\N)$ is dense in $s_\Phi$), then we say that $\Phi$ is \textit{regular}.
    \end{definition}

    Note that it is always true that $s_\Phi^{(0)}\subseteq s_\Phi$.

    \begin{example}
        We have that $\left\|\cdot \right\|_{\infty}$ is not regular, but for $1 \leq p < \infty$, $\left\|\cdot \right\|_{p}$ \textit{is} regular.
    \end{example}

    \begin{example}[Calder\'on Norm]
        Consider the following function for $p > 1$:
        \[
            \left\|a\right\|_{p,w} = \sup_{n} \left( n^{-1 + \frac{1}{p}}\sum_{j=1}^{n} a_j^{*} \right).
        \]
        This function is clearly symmetric. Further, by noting that it is the same as:
        \[
            \sup_{S \text{ a finite subset}} \left( |S|^{1 + \frac{1}{p}}\sum_{j\in S}^{} |a_j| \right),
        \]
        we easily verify that it is a norm. 
        %\textbf{If there is more time, talk about how this relates to the two weak $\ell^{p}$ spaces.}
    \end{example}

    \begin{theorem}\label{Thm1.16}
        Let $\Phi$ be a symmetric norm. Then
        \begin{enumerate}[label=(\alph*)]
            \item If $a_n \to 0$ as $n\to \infty$, then $\Phi(a) = \Phi(a^{*})$.
            \item If $a_n, b_n \to 0$ as $n\to \infty$, and if  $\sum_{n=1}^{N} b_n^{*} \leq \sum_{n=1}^{N} a_n^{*}$ for all $N = 1, 2, \ldots$ (in particular if $b_n^{*} \leq a_n^{*}$), then $\Phi(b) \leq \Phi(a)$.
            \item If $\Phi(1,0,0,\ldots) = c$, then
                \[
                    c\left\|a\right\|_{\infty} \leq \Phi(a) \leq c \left\|a\right\|_1.
                \]
                for any $a\in s_{\Phi}$.
            \item Both $s_{\Phi}$ and $s_\Phi^{(0)}$ are Banach spaces.
            \item If $\alpha$ is a doubly substochastic matrix and $a\in s_{\Phi}$ (resp. in $s_\Phi^{(0)}$), then $\alpha a$, defined by
                \[
                    (\alpha a)_n = \sum_{m}^{} \alpha_{nm}a_m
                \]
                is in $s_{\Phi}$ (resp. in $s_\Phi^{(0)}$) and $\phi(\alpha a) \leq \phi(a)$.
            \item If $\Phi$ is inequivalent to $\left\|\cdot \right\|_\infty$, then $s_{\Phi}$ only consists of sequences $a_n$ with $a_n \to 0$ as $n\to \infty$.
            \item If $s_\Phi = s_\Psi$, then $\Phi$ and $\Psi$ are equivalent norms.
        \end{enumerate}
    \end{theorem}

    For the proof we need the following results for which we will omit a proof (see, e.g. reference section, and class notes respectively):
    \begin{theorem}\label{Thm1.9}
        Let $a_1 \geq a_2 \geq \ldots \geq a_N \geq 0$ and let $b$ be a point in $\C^{N}$ with
        \begin{align}
            \sum_{j=1}^{k} b_j^{*} \leq \sum_{j=1}^{k} a_j\label{1.6}
        \end{align}
        for $k = 1, \ldots, N$. Then there exist points $a^{(1)}, \ldots, a^{(m)}\in \C^{N}$ with $(a^{l})^{*} = a$ for all $l = 1,\ldots,m$; and $0\leq \lambda_l \leq 1$ so that
        \[
            b = \sum_{l=1}^{m} \lambda_l a^{(l)}.
        \]
        In particular, if \eqref{1.6} holds and $\Phi$ is a convex, symmetric function on $\C^{N}$, then
        \[
            \Phi(b) \leq \Phi(a).
        \]
    \end{theorem}
    \begin{proposition}\label{Prop1.12}
        Let $\alpha$ be a doubly substochastic matrix. Let $c\in \C^{N}$ and $a = c^{*}$ and $b_n = \sum_{m=1}^{N} \alpha_{nm} c_m$. Then $b$ and $a$ obey \eqref{1.6}.
    \end{proposition}

    \begin{proof}[Proof of Theorem \ref{Thm1.16}]
        (a), (b). The proof of (b) for when $a_n$ and $b_n$ are finite sequences follows from Theorem \ref{Thm1.9}. To prove part (a), note that since
        \[
            \sum_{j=1}^{k} a_j^{*}\delta_{j \leq N} \leq \sum_{j=1}^{k} a_j^{*}\delta_{j \leq N+1} \leq \ldots,
        \]
        Theorem \ref{Thm1.9} says that 
        \[
            \Phi(a_1, \ldots, a_n, 0, 0, \ldots)
        \]
        is increasing with $n$, and further,
        \[
            \Phi(a_1, \ldots, a_n, 0, 0, \ldots) \leq \Phi(a_1^{*}, \ldots, a_n^{*}, 0,0,\ldots).
        \]
        Therefore, we get that $\Phi(a), \Phi (a^{*})$ exist and satisfy $\phi(a) \leq \Phi(a^{*})$. For the other inequality, for each $n$, fix $N$ so that
        \[
            \{a_1^{*}, \ldots, a_n^{*}\}  \subseteq \{|a_1|, \ldots, |a_N|\} .
        \]
        Then by using (b) for finite sequences (by padding the first sequence with zeros), we get that
        \[
            \Phi(a_1^{*}, \ldots, a_n^{*}, 0,0,\ldots) \leq \Phi(|a_1|, \ldots, |a_N|, 0,0,\ldots) = \Phi(a_1, \ldots, a_N, 0,0,\ldots).
        \]
        By taking limits (and monotonicity, symmetry), we get that $\Phi(a^{*})\leq \Phi(a)$. Thus, (a) is proven. Therefore, taking $a,b$ satisfying the hypotheses of (b), we have that
        \[
            \Phi(b_1^{*}, \ldots, b_N^{*}, 0,0,\ldots) \leq \Phi(a_1^{*}, \ldots, a_N^{*}, 0,0,\ldots).
        \] 
        By (a) we may take limits (since both of them exist), and further we then have that
        \[
            \Phi(b) = \Phi(b^{*}) \leq \Phi(a^{*}) = \Phi(a),
        \]
        as desired.

        (c) Using (a) and the triangle inequality, we have that for any $a\in s_{\Phi}$:
        \[
            c|a_n| = \Phi(0,\ldots,a_n,0,\ldots) \leq \Phi(a) \leq \sum_{n}^{} \Phi(0,\ldots,a_n,0,\ldots) = \sum_{n}^{} c|a_n|.
        \]
        By taking a supremum over $n$ in the first inequality we get the desired result.

        (d) Since $s_\Phi^{(0)}$ is a closed subspace of $s_\Phi$, we need only prove that $s_\Phi$ is Banach. All but completeness is clear. Let $a^{(m)}\in s_{\Phi}$ be Cauchy. By $c$, we have that $a_n^{(m)}$ is Cauchy for each fixed $n$, so take $a$ to be the pointwise limit:
        \[
            a_n^{(m)} \to a_n
        \] 
        for each $n$. Notice that
        \begin{align*}
            \Phi&(a_1 - a_1^{(m)}, \ldots, a_N - a_N^{(m)}, 0, \ldots)\\
            &= \lim_{n \to \infty} \Phi(a_1^{(n)} - a_1^{(m)}, \ldots, a_N^{(n)} - a_N^{(m)}, 0,\ldots)\\
            &\leq \lim_{n \to \infty} \Phi(a^{(n)} - a^{(m)}).
        \end{align*}
        where the inequality comes from the monotonicity proved by (b). It follows from monotonicity, that the limit of the LHS exists, and by the above inequality we see this limit is finite. Thus, $a\in s_{\Phi}$ and $\Phi(a - a^{(m)}) \to 0$.

        (e) Let $b = \alpha a$ and let $b^{(N)} = (b_1, \ldots, b_N, 0, \ldots).$ By the argument in Proposition~\ref{Prop1.12}, we have
        \[
            \sum_{j=1}^{k} b_j^{(N)} \leq \sum_{j=1}^{k} a_j^{*}.
        \]
        Therefore, $\Phi(b^{(N)}) \leq \Phi(a^{*})$ by (b). All that remains to show is that $\alpha$ takes $s_\Phi^{(0)}$ into $s_\Phi^{(0)}$. For this, it suffices to show that $\alpha a \in s_\Phi^{(0)}$ for $a\in c_{00}(\N)$. To this end, let $a = (a_1, \ldots, a_N, 0,0,\ldots)\in c_{00}(\N)$. Then only the first $N$ columns of $\alpha$ matter in the result of $\alpha a$. For fixed $\epsilon > 0$, take $M$ such that
        \[
            \sum_{i> M}^{} \left| \alpha_ij \right| \leq \epsilon, \text{ for }j = 1,\ldots, N.
        \]
        Then we have that
        \[
            \Phi(0,\ldots,(\alpha a)_{M+1},\ldots) \leq \epsilon \Phi(a) \to 0,
        \]
        so that $\alpha a \in s_{\Phi}^{(0)}$.

        (f) If $a_n\in s_\Phi$ and $a_n \not\to 0$, then take $\epsilon$ such that $|a_n| > \epsilon$ for infinitely many $n$. Then we have that for each sequence $(1,\ldots,1,0,0,\ldots)$ with $n$ many non-zero entries, there exists an $N$ such that the sequences:
        \[
            (1,\ldots,1,0,0,\ldots), \epsilon^{-1}(a_1, a_2, \ldots, a_N, 0,0,\ldots)
        \]
        satisfy the conditions of (b). Therefore, we get that
        \begin{align*}
            \Phi(1,\ldots,1,0,0,\ldots)
            &\leq \epsilon^{-1}\Phi(a_1, \ldots, a_N, 0,0,\ldots)\\
            &\leq \epsilon^{-1}\Phi(a_1, \ldots, a_{N+1}, 0,0,\ldots ) \leq \ldots \leq \epsilon^{-1}\Phi(a).
        \end{align*}
        Therefore, we have that (by monotonicity) that
        \[
            \lim_{n \to \infty} \Phi(1,\ldots,1,0,0,\ldots) \hspace{10pt} \text{($n$ many non-zero entries)}
        \]
        exists and is finite, so that $(1,1,\ldots)\in s_{\Phi}$. Further, we have that
        \[
            \left\|a\right\|_{\infty}\Phi(1,1,\ldots) \geq \Phi(a),
        \]
        and the other inequality comes from (c).

        (g) Then we have that By $c$, the identity map from $s_\Phi$ to $s_\Psi$ is closed, so $\Phi$ is equivalent to $\Psi$ by the Closed Graph Theorem.
    \end{proof}

    \subsection{Conjugate Norms}
    Given a symmetric norm $\Phi$, we define the norm $\Phi'$ on $c_{00}(\N)$ by:
    \[
        \Phi'(b) = \sup \left\{ \left| \sum_{n}^{} a_n b_n \right|  : a\in c_{00}(\N), \Phi(a) \leq 1\right\}.
    \]
    Note that since we can rearrange the sum, this is the same as
    \[
        \Phi'(b) = \sup \left\{ \left| \sum_{n}^{} a_n b_n^{*} \right|  : a\in c_{00}(\N), \Phi(a) \leq 1\right\}.
    \]
    Thus it is clear that $\Phi'$ is symmetric, and all of the properties of a norm are clear except for the triangle inequality. This becomes clear by using the second expression, along with noting that this sup will be acheived by some element $a\in c_{00}(\N)$ with $a = a^{*}$. Therefore, we have that
    \begin{align*}
        \Phi'(b + c) 
        &= \sup \left\{ \sum_{n}^{} a_n (b_n + c_n)^{*}  : a\in c_{00}(\N), \Phi(a) \leq 1, a = a^{*}\right\}\\
        &\leq \sup \left\{  \sum_{n}^{} a_n b_n^{*}  + \sum_{n}^{} a_n c_n^{*}  : a\in c_{00}(\N), \Phi(a) \leq 1, a = a^{*}\right\}\\
        &\leq \sup \left\{  \sum_{n}^{} a_n b_n^{*} : a\in c_{00}(\N), \Phi(a) \leq 1, a = a^{*}\right\} \\
        &\quad + \sup \left\{  \sum_{n}^{} a_n c_n^{*} : a\in c_{00}(\N), \Phi(a) \leq 1, a = a^{*}\right\}\\
        &= \Phi'(b) + \Phi'(c).
    \end{align*}


    \begin{theorem}[Schatten]\label{Thm1.17}
       Fix a symmetric norm $\Phi$. Then,
       \begin{enumerate}[label=(\alph*)]
           \item $\sum_{}^{} \left| a_n b_n \right| \leq \Phi(a)\Phi'(b)$.
           \item $\left( s_\Phi^{(0)} \right)^{*} = s_{\Phi'}$ (including norm) in the sense that any continuous linear functional on $s_\Phi$ has the form 
               \[
                   a \mapsto \sum_{n}^{} a_n b_n
               \]
               for some $b\in s_{\Phi'}$.
           \item $s_\Phi^{(0)}$ (resp. $s_\Phi$) is reflexive if and only if $\Phi$ and $\Phi'$ are both regular.
       \end{enumerate}
    \end{theorem}
    For the proof we need the following Lemma which we will give without proof (Hint: telescope the sum using $|a_k| - |a_{k+1}|$).
    \begin{lemma}\label{Lem1.8}
        \[
            \sum_{n}^{} \left| a_n b_n \right| \leq \sum_{n}^{} a_n^{*}b_n^{*}.
        \]
    \end{lemma}
    \begin{proof}[Proof of Theorem \ref{Thm1.17}]
        (a) For $N = 1,2,\ldots$, we have that 
        \begin{align*}
            \sum_{n=1}^{N} \left| a_n b_n \right|
            &\leq \sum_{n=1}^{N} a_n^{*} b_n^{*}\\
            &\leq \Phi(a_1^{*}, \ldots, a_N^{*}, 0,\ldots) \Phi'(b_1^{*}, \ldots, b_N^{*}, 0,\ldots)\\
            &\leq \Phi(a) \Phi'(b).
        \end{align*}
        Here, we used Lemma \ref{Lem1.8} for the first inequality (along with the fact that the $*$ of a truncated sequence is no smaller than the $*$ of the untruncated version), the definition of $\Phi'$ in the second, and monotonicity following from Theorem \ref{Thm1.16} in the final inequality. The result follows by taking the limit.

        (b) By (a), every $b$ defines a \emph{bounded} linear functional. Further, by compactness of closed and bounded subspaces of $\C^{N}$, we have that there is an $a\in c_{00}(\N)$ (with at most $N$ many non-zero entries) such that $\Phi(a) = 1$ and \[
            \left| \sum_{n}^{} a_n b_n \right| = \Phi'(b_1,\ldots,b_N,0,\ldots),
        \]
        so that $\Phi'(b)$ is the norm of $b$ as a linear functional. Thus, we need only show that \textit{any} bounded linear functional on $s_\Phi$ is of the form
        \[
            l(a) = \sum_{n}^{} a_n b_n
        \]
        for some fixed $b\in s_{\Phi'}$. Since $(\C^{n})^{*} = \C^{n}$, we know that there exists a sequence $b$ such that
        \[
            l(a) = \sum_{n}^{} a_n b_n
        \]
        for each $a\in c_{00}(\N)$. Moreover, by definition of $\Phi'$, we have
        \[
            \Phi'(b_1, \ldots, b_n, 0,\ldots) = \sup\left\{ l(a) \mid a \in c_{00}(\N), \Phi(a) \leq 1 \right\} \leq \left\|b\right\|,
        \]
        so indeed $b\in s_{\Phi'}$. Since $l = b$ on $c_00(\N),$ we know that $l = b$ on $s_\Phi^{(0)}$.

        (c) If $\Phi$ and $\Phi'$ are regular, then
        \[
            (s_\Phi^{(0)})^{* *} = (s_{\Phi'})^{*} = (s_{\Phi'}^{(0)})^{*} = s_\Phi = s_\Phi^{(0)}.
        \]
         So that both $s_\Phi$ and $s_\Phi^{(0)}$ are reflexive. If $\Phi'$ is not regular, then there is a linear functional which vanishes on $s_{\Phi'}^{(0)}$, but not on $s_{\Phi'}$ (by Hahn-Banach, since $s_{\Phi'}^{(0)}$ is closed in $s_{\Phi'}$, so we may extend $s_{\Phi'}^{(0)}$ by a 1-dimensional subspace of $s_{\Phi'}$ which is not contained in $s_{\Phi'}^{(0)}$, define a linear functional non-trivially on this finite extension, and then extend it to all of $s_{\Phi'}$ by Hahn-Banach). If $\Phi'$ is regular, but $\Phi$ is not, then we have
         \[
             (s_\Phi^{(0)})^{* *} = (s_{\Phi'})^{*} = (s_{\Phi'}^{(0)})^{*} = s_\Phi \supsetneq s_\Phi^{(0)}.
         \]
    \end{proof}
    

    \bibliographystyle{alpha}
    %\bibliography{/home/nco/nextcloud/master.bib}
    \bibliography{./localbib.bib}
\end{document}
